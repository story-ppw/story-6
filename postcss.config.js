module.exports = {
	plugins: [
		require('tailwindcss')('./tailwind.config.js'),
		require('@fullhuman/postcss-purgecss')({
			content: [
				'./story_6/apps/activity/templates/activity/*.html',
				'./story_6/templates/*.html',
				'./story_6/apps/story_4/templates/story_4/*.html',
				'./story_6/apps/schedule/templates/schedule/*.html',
				'./story_6/apps/story_1/templates/story_1/*.html',
			],
			defaultExtractor: content => content.match(/[A-za-z0-9-_:/]+/g) || []
		})
	]
}
