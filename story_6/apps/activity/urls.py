"""story_6 URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from . import views

app_name = 'activity'

urlpatterns = [
    path('', views.index, name="index"),
    path('add-activity', views.add_activity, name="add_activity"),
    path('add-participant/<int:id>', views.add_participant, name="add_participant"),
    path('remove-activity/<int:id>', views.remove_activity, name="remove_activity"),
    path('remove-participant/<int:id>', views.remove_participant, name="remove_participant"),
]
