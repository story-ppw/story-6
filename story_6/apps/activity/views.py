from django.shortcuts import render, get_object_or_404
from django.http import HttpResponseRedirect
from django.contrib import messages
from .forms import ActivityForm, ParticipantForm
from .models import Activity, Participant


# Create your views here.

def index(request):
    response = {
        'activity_form': ActivityForm,
        'activities': Activity.objects.all(),
        'participants': Participant.objects.all(),
        'participant_form': ParticipantForm
    }

    return render(request, 'activity/index.html', response)

def add_activity(request):
    form = ActivityForm(request.POST or None)
    if form.is_valid():
        name = request.POST['name']
        form.save()
        messages.success(request, f'{name} has been added!')
    return HttpResponseRedirect('/activity')

def remove_activity(request, id):
    activity = get_object_or_404(Activity, pk=id)
    activity.delete()
    messages.success(request, f'{activity.name} has been deleted')
    return HttpResponseRedirect('/activity')

def add_participant(request, id):
    form = ParticipantForm(request.POST or None)
    if form.is_valid():
        name = request.POST['name']
        activity = get_object_or_404(Activity, pk=id)
        Participant.objects.create(name=name, activity=activity)
        messages.success(request, f'{name} has been added to {activity.name}')
    return HttpResponseRedirect('/activity')

def remove_participant(request, id):
    participant = get_object_or_404(Participant, pk=id)
    messages.success(request, f'{participant} has been deleted from {participant.activity.name}')
    participant.delete()
    return HttpResponseRedirect('/activity')
