from django import forms
from .models import Activity, Participant


class ActivityForm(forms.ModelForm):
    class Meta:
        model = Activity
        fields = '__all__'

        labels = {
            'name': 'Activity',
        }

        widgets = {
            'name': forms.TextInput(attrs={'placeholder': 'Activity Name'}),
            'description': forms.Textarea(attrs={'placeholder': 'Add Some Description'}),
        }


class ParticipantForm(forms.ModelForm):
    class Meta:
        model = Participant
        fields = ['name']

        widgets = {
            'name': forms.TextInput(attrs={'placeholder': 'Your Name'})
        }
