from django.test import TestCase
from django.urls import resolve
from story_6.apps.activity.models import Activity, Participant
from story_6.apps.activity.views import index

class UnitTest(TestCase):

    def test_activity_page_exist(self):
        response = self.client.get('/activity/')
        self.assertEqual(response.status_code, 200)

    def test_activity_page_function(self):
        found = resolve('/activity/')
        self.assertEqual(found.func, index)

    def test_activity_template(self):
        response = self.client.get('/activity/')
        self.assertTemplateUsed(response, 'activity/index.html')

    def test_add_activity(self):
        count = Activity.objects.all().count()
        response = self.client.post(
            '/activity/add-activity', {'name': 'Test', 'description': 'Testing'})
        self.assertEqual(Activity.objects.all().count(), count + 1)
        self.assertEqual(response.status_code, 302)

    def test_add_participant(self):
        count = Participant.objects.all().count()
        activity = Activity.objects.create(name='Test', description='Testing')
        response = self.client.post(f'/activity/add-participant/{activity.id}', {'name':'PPW', 'activity' : activity})
        self.assertEqual(Participant.objects.all().count(), count + 1)
        self.assertEqual(response.status_code, 302)

    def test_remove_activity(self):
        activity = Activity.objects.create(name='Test', description='Testing')
        count = Activity.objects.all().count()
        response = self.client.get(f'/activity/remove-activity/{activity.id}')
        self.assertEqual(Activity.objects.all().count(), count - 1)
        self.assertEqual(response.status_code, 302)

    def test_remove_participant(self):
        activity = Activity.objects.create(name="Test", description="Testing")
        participant = Participant.objects.create(name="PPW", activity=activity)
        count = Participant.objects.all().count()
        response = self.client.get(f'/activity/remove-participant/{participant.id}')
        self.assertEqual(Participant.objects.all().count(), count - 1)
        self.assertEqual(response.status_code, 302)
