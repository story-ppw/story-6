from django import forms
from story_6.apps.schedule.models import Course
from django.forms import TextInput, NumberInput, Textarea, Select

class CourseForm(forms.ModelForm):
    class Meta:
        model = Course
        fields = "__all__"
        labels = {
            'name': 'Course',
            'year_parity': 'Term',
        }
        widgets = {
            'name': TextInput(attrs={'placeholder': 'Course Name'}),
            'lecturer': TextInput(attrs={'placeholder': 'Lecturer Name'}),
            'credit': NumberInput(attrs={'placeholder': 'How Many Credit'}),
            'description': Textarea(attrs={'placeholder': 'Some Description About The Course'}),
            'year_parity': Select(),
            'start_year': NumberInput(attrs={'placeholder': 'Term Starting Year, E.g 2019'}),
            'room': TextInput(attrs={'placeholder': 'Room Name Or Number'}),
        }
