# Story 6

[![pipeline status][pipeline-badge]][commit]
[![coverage report][coverage-badge]][commit]

Repo ini akan digunakan untuk story 6

[pipeline-badge]: https://gitlab.com/story-ppw/story-6/badges/master/pipeline.svg
[coverage-badge]: https://gitlab.com/story-ppw/story-6/badges/master/coverage.svg
[commit]: https://gitlab.com/story-ppw/story-6/-/commits/master
